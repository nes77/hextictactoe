__author__ = 'Nicholas'

from tictactoe.core import *
import random as rand


class SusanAI(Player):
    def __init__(self):
        self.majorver = 0
        self.minorver = 1
        self.releasever = 0
        self.name = 'Susan'
        rand.seed()

    def __getname__(self):
        return '{0}, version {1}.{2}.{3}'.format(self.name, self.majorver, self.minorver, self.releasever)

    def think(self, state, priority):


    def __nextmove__(self, state):
        if set(state.data) == {'b'}:
            return rand.randint(0, 8)
        else:
            return self.think(state, self.priority)
