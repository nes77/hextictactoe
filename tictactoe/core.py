__author__ = 'Nicholas'

from abc import ABCMeta, abstractmethod


class GameState(object):
    def __init__(self):
        self.data = ['b', 'b', 'b',
                     'b', 'b', 'b',
                     'b', 'b', 'b']

    def __str__(self):
        retstr = 'Game State:\n' \
                 '{0} | {1} | {2}\n' \
                 '{3} | {4} | {5}\n' \
                 '{6} | {7} | {8}\n'.format(self.data[0], self.data[1], self.data[2], self.data[3], self.data[4],
                                            self.data[5], self.data[6], self.data[7], self.data[8])

        x = sum([1 if temp is 'x' else 0 for temp in self.data])
        o = sum([1 if temp is 'o' else 0 for temp in self.data])

        otherstr = ''
        #'x has {0} squares.\no has {1} squares.'.format(x, o)
        retstr = '{0}{1}'.format(retstr, otherstr)

        return retstr

    def altergamestate(self, tilenum, changeto):
        self.data[tilenum] = changeto

    def tileisfree(self, tilenum):
        return self.data[tilenum] is 'b'

    @staticmethod
    def hasthreeinrow(data):
        if data[0:3] == ['x', 'x', 'x'] or data[0:3] == ['o', 'o', 'o']:
            return True
        elif data[3:6] == ['x', 'x', 'x'] or data[3:6] == ['o', 'o', 'o']:
            return True
        elif data[6:9] == ['x', 'x', 'x'] or data[6:9] == ['o', 'o', 'o']:
            return True
        elif [data[0], data[3], data[6]] == ['x', 'x', 'x'] or [data[0], data[3], data[6]] == ['o', 'o', 'o']:
            return True
        elif [data[1], data[4], data[7]] == ['x', 'x', 'x'] or [data[1], data[4], data[7]] == ['o', 'o', 'o']:
            return True
        elif [data[2], data[5], data[8]] == ['x', 'x', 'x'] or [data[2], data[5], data[8]] == ['o', 'o', 'o']:
            return True
        elif [data[0], data[4], data[8]] == ['x', 'x', 'x'] or [data[0], data[4], data[8]] == ['o', 'o', 'o']:
            return True
        elif [data[2], data[4], data[6]] == ['x', 'x', 'x'] or [data[2], data[4], data[6]] == ['o', 'o', 'o']:
            return True
        else:
            return False

    def isunwinnablestate(self):
        testx = ['x' if elem is 'b' else elem for elem in self.data]
        testo = ['o' if elem is 'b' else elem for elem in self.data]
        return not (GameState.hasthreeinrow(testx) or GameState.hasthreeinrow(testo))

    def isgameoverstate(self):
        """
        Tests to see if a player has won the game
        :return True if the game is over, and a string containing who won:
        """
        if self.data[0:3] == ['x', 'x', 'x']:
            return True, 'x'
        elif self.data[0:3] == ['o', 'o', 'o']:
            return True, 'o'
        elif self.data[3:6] == ['x', 'x', 'x']:
            return True, 'x'
        elif self.data[3:6] == ['o', 'o', 'o']:
            return True, 'o'
        elif self.data[6:9] == ['x', 'x', 'x']:
            return True, 'x'
        elif self.data[6:9] == ['o', 'o', 'o']:
            return True, 'o'
        elif [self.data[0], self.data[3], self.data[6]] == ['x', 'x', 'x']:
            return True, 'x'
        elif [self.data[1], self.data[4], self.data[7]] == ['x', 'x', 'x']:
            return True, 'x'
        elif [self.data[2], self.data[5], self.data[8]] == ['x', 'x', 'x']:
            return True, 'x'
        elif [self.data[0], self.data[3], self.data[6]] == ['o', 'o', 'o']:
            return True, 'o'
        elif [self.data[1], self.data[4], self.data[7]] == ['o', 'o', 'o']:
            return True, 'o'
        elif [self.data[2], self.data[5], self.data[8]] == ['o', 'o', 'o']:
            return True, 'o'
        elif [self.data[0], self.data[4], self.data[8]] == ['x', 'x', 'x']:
            return True, 'x'
        elif [self.data[0], self.data[4], self.data[8]] == ['o', 'o', 'o']:
            return True, 'o'
        elif [self.data[2], self.data[4], self.data[6]] == ['o', 'o', 'o']:
            return True, 'o'
        elif [self.data[2], self.data[4], self.data[6]] == ['x', 'x', 'x']:
            return True, 'x'
        elif self.isunwinnablestate():
            return True, None
        else:
            return False, None


class Player:
    __metaclass__ = ABCMeta

    def __init__(self):
        self.name = self.__getname__
        self.priority = 0

    @abstractmethod
    def __nextmove__(self, state):
        raise NotImplementedError('Must implement player logic')

    @abstractmethod
    def __getname__(self):
        return 'abstract'

    def setpriority(self, priority):
        self.priority = priority


class HumanPlayer(Player):
    def __nextmove__(self, state):
        rettile = 0
        if self.priority is 1:
            retchar = 'x'
        else:
            retchar = 'o'
        print(state)
        print('1 | 2 | 3\n'
              '4 | 5 | 6\n'
              '7 | 8 | 9')
        print('Player {0}, enter a tile number. You must select a free tile (marked with\n'
              'a "b"). Your tiles are represented by {1}.'.format(self.priority, retchar))

        goodinput = False

        while not goodinput:
            try:
                play = raw_input('Your move: ')
                print('')
                rettile = int(play)
                if 1 <= rettile <= 9 and state.tileisfree(rettile - 1):
                    goodinput = True
                else:
                    raise ValueError('You must enter a valid integer between 1 and 9, inclusive. This tile must represe'
                                     'nt\na free tile (one marked with "b")')
            except ValueError:
                print('Value not accepted, try again.')
                print(state)

        return rettile - 1

    def __getname__(self):
        return 'HumanPlayer'


class Game(object):
    def __init__(self, player1, player2):
        """
        Starts a new game of Tic-Tac-Toe with the given players.
        :type player1: Player
        :type player2: Player
        """
        self.player1 = player1
        player1.setpriority(1)
        self.player2 = player2
        player2.setpriority(2)
        self.gamestate = GameState()

    def rungame(self):
        print('Player 1 is a {0}'.format(self.player1.__getname__()))
        print('Player 2 is a {0}'.format(self.player2.__getname__()))
        isover, whowon = self.gamestate.isgameoverstate()
        while not isover:
            self.gamestate.altergamestate(self.player1.__nextmove__(self.gamestate), 'x')
            isover, whowon = self.gamestate.isgameoverstate()
            if not isover:
                self.gamestate.altergamestate(self.player2.__nextmove__(self.gamestate), 'o')
                isover, whowon = self.gamestate.isgameoverstate()

        print(self.gamestate)
        if whowon is 'x':
            winner = 1
            print('The game is over. Player {0} wins.'.format(winner))
        elif whowon is 'o':
            winner = 2
            print('The game is over. Player {0} wins.'.format(winner))
        else:
            print('The game is over. No one can win.')
            return



